ID_LIST=`curl --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines?ref=${CI_COMMIT_REF_NAME}&scope=running"| grep -Eo '"(id)":([^,]+)'|sed -e 's/"id"://'`
for id in $ID_LIST; do
  if [ "$id" -lt "$CI_PIPELINE_ID" ]; then
    echo $id;
    curl --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${id}/cancel"
  fi
done
